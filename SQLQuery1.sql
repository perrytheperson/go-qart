﻿CREATE TABLE [dbo].[Student] (
    [ENumber]       VARCHAR (50) NOT NULL,
    [FirstName]     VARCHAR (50) NULL,
    [LastName]      VARCHAR (50) NULL,
    [Concentration] VARCHAR (80) NULL,
    PRIMARY KEY CLUSTERED ([ENumber] ASC)
);

CREATE TABLE [dbo].[Course] (
    [Course_ID]     INT          IDENTITY (1, 1) NOT NULL,
    [Course_Name]   VARCHAR (50) NULL,
    [Course_Number] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Course_ID] ASC)
);

CREATE TABLE [dbo].[Exam] (
    [Exam_ID] INT          IDENTITY (1, 1) NOT NULL,
    [Date]    DATE         NULL,
    [Score]   INT          NULL,
    [ENumber] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Exam_ID] ASC),
    CONSTRAINT [FK_StudentExam] FOREIGN KEY ([ENumber]) REFERENCES [dbo].[Student] ([ENumber])
);



CREATE TABLE [dbo].[Question] (
    [Question_ID]      INT           IDENTITY (1, 1) NOT NULL,
    [QuestionText]     VARCHAR (500) NOT NULL,
    [ExpectedResponse] VARCHAR (500) NOT NULL,
    [Course_ID]        INT           NOT NULL,
    [Semester]         VARCHAR (15)  NOT NULL,
    [Year]             INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Question_ID] ASC),
    CONSTRAINT [FK_QuestionCourse] FOREIGN KEY ([Course_ID]) REFERENCES [dbo].[Course] ([Course_ID])
);

CREATE TABLE [dbo].[StudentQuestion] (
    [ENumber]     VARCHAR (50) NOT NULL,
    [Question_ID] INT          NOT NULL,
    [Score]       INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([ENumber] ASC, [Question_ID] ASC)
);

CREATE TABLE [dbo].[ExamQuestion] (
    [Exam_ID]     INT NOT NULL,
    [Question_ID] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Exam_ID] ASC, [Question_ID] ASC)
);

